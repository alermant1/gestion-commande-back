<?php

namespace App\Service;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\CustomNormalizer;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;

class SerializerService
{
    private $serializer;

    public function __construct()
    {
        $this->serializer = new Serializer([
            new DateTimeNormalizer('Y-m-d'),
            new JsonSerializableNormalizer(),
            new ObjectNormalizer(null, null, null, new ReflectionExtractor()),
        ], [
            new JsonEncoder(),
        ]);
    }

    public function deserialize($content, $class)
    {
        return $this->serializer->deserialize($content, $class, 'json');
    }

    public function serialize($object, $class)
    {
        return $this->serializer->serialize($object, 'json');
    }
}