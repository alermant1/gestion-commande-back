<?php

namespace App\Service;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;

class PdfService
{
    public function __construct(Pdf $pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * Génère et retourne le pdf.
     *
     * @param string $html
     * @param string $fileName
     *
     * @return PdfResponse
     */
    public function getPdfResponse($html, $fileName, $options) : PdfResponse
    {
        $options = array_merge([
            "encoding" => "UTF-8",
            "viewport-size" => "1920x1080",
            "footer-right" => "[page]/[toPage]",
            "footer-font-size" => 10,
        ], $options);

        return new PdfResponse(
            $this->pdf->getOutputFromHtml($html, $options),
            $fileName
        );
    }
}