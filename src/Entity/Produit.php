<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProduitRepository")
 * @Orm\Table(name="gestion__produit")
 */
class Produit implements \JsonSerializable
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $format;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ml;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mlFinition;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $moulure;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $moulureFinition;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prix;

    /**
     * @var Commande
     * @ORM\ManyToOne(targetEntity="App\Entity\Commande", inversedBy="produits")
     */
    private $commande;

    /**
     * @var string
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $ordre;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    public function getId()
    {
        return $this->id;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(?string $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getMl(): ?string
    {
        return $this->ml;
    }

    public function setMl(?string $ml): self
    {
        $this->ml = $ml;

        return $this;
    }

    public function getMlFinition(): ?string
    {
        return $this->mlFinition;
    }

    public function setMlFinition(?string $mlFinition): self
    {
        $this->mlFinition = $mlFinition;

        return $this;
    }

    public function getMoulure(): ?string
    {
        return $this->moulure;
    }

    public function setMoulure(?string $moulure): self
    {
        $this->moulure = $moulure;

        return $this;
    }

    public function getMoulureFinition(): ?string
    {
        return $this->moulureFinition;
    }

    public function setMoulureFinition(?string $moulureFinition): self
    {
        $this->moulureFinition = $moulureFinition;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(?string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getCommande(): ?Commande
    {
        return $this->commande;
    }

    public function setCommande(?Commande $commande): self
    {
        $this->commande = $commande;

        return $this;
    }

    public function getOrdre()
    {
        return $this->ordre;
    }
    public function setOrdre($ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->getId(),
            "quantite" => $this->getQuantite(),
            "format" => $this->getFormat(),
            "ml" => $this->getMl(),
            "mlFinition" => $this->getMlFinition(),
            "moulure" => $this->getMoulure(),
            "moulureFinition" => $this->getMoulureFinition(),
            "prix" => $this->getPrix(),
            "ordre" => $this->getOrdre(),
        ];
    }
}
