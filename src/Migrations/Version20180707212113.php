<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180707212113 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE gestion__commande (id INT AUTO_INCREMENT NOT NULL, id_client_id INT DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, date DATE NOT NULL, INDEX IDX_6EEAA67D99DED506 (id_client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE gestion__commande ADD CONSTRAINT FK_6EEAA67D99DED506 FOREIGN KEY (id_client_id) REFERENCES gestion__client (id)');
        $this->addSql('ALTER TABLE gestion__produit ADD commande_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE gestion__produit ADD CONSTRAINT FK_29A5EC2782EA2E54 FOREIGN KEY (commande_id) REFERENCES gestion__commande (id)');
        $this->addSql('CREATE INDEX IDX_29A5EC2782EA2E54 ON gestion__produit (commande_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE gestion__produit DROP FOREIGN KEY FK_29A5EC2782EA2E54');
        $this->addSql('DROP TABLE gestion__commande');
        $this->addSql('DROP INDEX IDX_29A5EC2782EA2E54 ON gestion__produit');
        $this->addSql('ALTER TABLE gestion__produit DROP commande_id');
    }
}
