<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Client;
use App\Entity\User;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $clients = [
            ["Johnny", "raison social", "2 rue des patates", "94000", "Créteil"],
            ["Bobby", "pas social", "10 rue de Paris", "750017", "Paris"],
            ["Stevy", "social", "2 rue des noobs", "13000", "Marseille"],
            ["Mick", "une raison", "50 avenue du Général Pierre Biollette", "94000", "Créteil"],
            ["Kevin", "sans raison", "33 rue jules ferry", "93000", "Saint denis"],
        ];
        foreach ($clients as $client) {
            $oClient = new Client();
            $oClient->setNom($client[0]);
            $oClient->setRaisonSocial($client[1]);
            $oClient->setAdresse($client[2]);
            $oClient->setCodePostal($client[3]);
            $oClient->setVille($client[4]);
            $manager->persist($oClient);
        }

        $user = new User();
        $user->setUsername("user");
        $user->setPassword("81bbab670c5fa2ec9d2be1f6d5f24d601ff53626beabd23979ff4ff84e872ec3");
        $user->setApiKey(".");

        $manager->persist($user);

        $manager->flush();
    }
}
