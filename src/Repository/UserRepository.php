<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    const ALGO_HMAC = "SHA256";
    public function __construct(RegistryInterface $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, User::class);
        $this->em = $em;
    }

    /**
     * Génère un token si le login/mdp est bon
     *
     * @return String $token
     *
     * @throws Exception
     */
    public function getTokenLogin(String $username, String $password, String $secret): String
    {
        try {
            $password = hash_hmac(self::ALGO_HMAC, $password, $secret);

            $user = $this->findOneBy([
                'username' => $username,
                'password' => $password
            ]);

            if (!$user instanceof User) {
                throw new \Exception("Clef reçu incorrecte.");
            }

            if ($user->getApiKey() === "") {
                $user->setApiKey($this->getRandomApiKey());
                $this->em->merge($user);
                $this->em->flush();
            }

        } catch (\Exception $e) {
            throw new \Exception("Erreur critique.");
        }

        return $user->getApiKey();
    }

    /**
     * Actualise le token
     *
     * @return String $token
     *
     * @throws Exception
     */
    public function refreshToken(String $apiKey): String
    {
        try {
            $user = $this->findOneBy(['apiKey' => $apiKey]);
            if (!$user instanceof User) {
                throw new \Exception("Clef reçu incorrecte.");
            }
            $user->setApiKey($this->getRandomApiKey());
            $this->em->merge($user);
            $this->em->flush();

        } catch (\Exception $e) {
            throw new \Exception("Erreur critique.");
        }

        return $user->getApiKey();
    }

    private function getRandomApiKey() : String
    {
        return base64_encode(microtime() . openssl_random_pseudo_bytes(64));
    }
}
