<?php

namespace App\Repository;

use DateTime;
use App\Entity\Commande;
use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Commande|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commande|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commande[]    findAll()
 * @method Commande[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Commande::class);
    }

    /**
     * @return Collection|Commande[]
     */
    public function findByClientOrderByDate(Client $client): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.client = :client')
            ->setParameter('client', $client)
            ->orderBy('c.date', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Collection|Commande[]
     */
    public function findAllOrderByDate() : array
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.date', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param DateTime $date
     *
     * @return Collection|Commande[]
     */
    public function findCommandesDuJour(DateTime $date): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('DATE(c.date) = DATE(:date)')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }
}
