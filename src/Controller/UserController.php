<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Service\SerializerService;
use App\Repository\UserRepository;
use App\Entity\User;

/**
 * @Route("/user")
 */
class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Génère et retourne un token si le login et mot de passe reçu est correcte.
     *
     * @Route("/token", methods={"POST"})
     */
    public function getToken(Request $request)
    {
        $content = json_decode($request->getContent());
        if (!isset($content->username, $content->password)) {
            return $this->json([], Response::HTTP_BAD_REQUEST);
        }

        try {
            $token = $this->userRepository->getTokenLogin(
                $content->username,
                $content->password,
                $this->container->getParameter('secretHmac')
            );
            return $this->json(["token" => $token]);
        } catch (\Exception $e) {
            return $this->json([], Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Retourne un nouveau token généré si le token reçu est correcte.
     *
     * @Route("/refresh", methods={"POST"})
     */
    public function getRefreshToken(Request $request)
    {
        $content = json_decode($request->getContent());
        if (!isset($content->currentToken)) {
            return $this->json([], Response::HTTP_BAD_REQUEST);
        }

        try {
            $token = $this->userRepository->refreshToken($content->currentToken);
            return $this->json(["token" => $token]);
        } catch (\Exception $e) {
            return $this->json([], Response::HTTP_FORBIDDEN);
        }
    }

}
