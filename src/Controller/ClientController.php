<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Service\SerializerService;
use App\Repository\CommandeRepository;
use App\Repository\ClientRepository;
use App\Entity\Client;
use App\Entity\Commande;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/api/client")
 */
class ClientController extends Controller
{
    private $clientRepository;
    private $serializer;
    private $em;

    public function __construct(
        ClientRepository $clientRepository,
        CommandeRepository $commandeRepository,
        SerializerService $serializerService,
        EntityManagerInterface $em
    ) {
        $this->clientRepository = $clientRepository;
        $this->commandeRepository = $commandeRepository;
        $this->serializerService = $serializerService;
        $this->em = $em;
    }

    /**
     * Récupère tous les clients.
     *
     * @Route("/", methods={"GET"})
     */
    public function getClients()
    {
        return $this->json($this->clientRepository->findAll());
    }

    /**
     * Récupère un client.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, methods={"GET"})
     */
    public function getOne($id)
    {
        return $this->json(
            $this->clientRepository->find($id)
        );
    }

    /**
     * Met à jour un client.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, methods={"PUT"})
     */
    public function updateOne(Request $request, $id)
    {
        $content = $request->getContent();

        $client = $this->serializerService->deserialize($content, Client::class);
        $client->setId($id);
        if (!$this->clientRepository->find($id) instanceof Client) {
            return $this->json([], 404);
        }

        $client = $this->em->merge($client);
        $this->em->flush();

        return $this->json($client, 200);
    }

    /**
     * Ajoute un client.
     *
     * @Route("/", methods={"POST"})
     */
    public function addOne(Request $request)
    {
        $content = $request->getContent();

        $client = $this->serializerService->deserialize($content, Client::class);

        $this->em->persist($client);
        $this->em->flush();

        return $this->json($client);
    }

    /**
     * Supprime un client.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, methods={"DELETE"})
     */
    public function deleteOne($id)
    {
        try {
            $client = $this->clientRepository->find($id);
            if (!$client instanceof Client) {
                return $this->json([], 404);
            }

            $this->em->remove($client);
            $this->em->flush();
            return $this->json([], 200);
        } catch (\Exception $e) {
            return $this->json([], 500);
        }
    }

    /**
     * Récupère les commandes d'un client.
     *
     * @Route("/{id}/commande", requirements={"id" = "\d+"}, methods={"GET"})
     */
    public function getCommandeClient($id)
    {
        $client = $this->clientRepository->find($id);
        if (!$client instanceof Client) {
            return $this->json([], 404);
        }

        return $this->json($this->commandeRepository->findByClientOrderByDate($client));
    }
}
