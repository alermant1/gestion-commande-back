<?php

namespace App\Controller;

use DateTime;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\SerializerService;
use App\Repository\ClientRepository;
use App\Repository\CommandeRepository;
use App\Repository\ProduitRepository;
use App\Entity\Client;
use App\Entity\Commande;
use App\Entity\Produit;
use App\Service\PdfService;
/**
 * @Route("/api/commande")
 */
class CommandeController extends Controller
{

    private $clientRepository;
    private $commandeRepository;
    private $produitRepository;
    private $serializer;
    private $em;

    public function __construct(
        ClientRepository $clientRepository,
        CommandeRepository $commandeRepository,
        ProduitRepository $produitRepository,
        SerializerService $serializerService,
        EntityManagerInterface $em,
        PdfService $pdfService
    ) {
        $this->clientRepository = $clientRepository;
        $this->commandeRepository = $commandeRepository;
        $this->serializerService = $serializerService;
        $this->produitRepository = $produitRepository;
        $this->em = $em;
        $this->pdfService = $pdfService;
    }

    /**
     * Récupère toutes les commandes triés par date dans l'ordre décroissant.
     *
     * @Route("/", methods={"GET"})
     */
    public function getCommandes()
    {
        return $this->json($this->commandeRepository->findAllOrderByDate());
    }

    /**
     * Récupère les commandes du jour.
     *
     * @Route("/date/{date}", methods={"GET"})
     */
    public function getCommandesDuJour(DateTime $date)
    {
        return $this->json($this->commandeRepository->findCommandesDuJour($date));
    }

    /**
     * Recupère une commande.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, methods={"GET"})
     */
    public function getOne($id)
    {
        return $this->json(
            $this->commandeRepository->find($id)
        );
    }

    /**
     * Met à jour une commande.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, methods={"PUT"})
     */
    public function updateOne(Request $request, $id)
    {
        $content = $request->getContent();

        $commande = $this->serializerService->deserialize($content, Commande::class);
        $commande->setId($id);

        // on vérifie que la commande existe
        if (!$this->commandeRepository->find($id) instanceof Commande) {
            return $this->json(["data" => "La commande n'existe pas."], 404);
        }

        if (!$commande->getClient() instanceof Client) {
            return $this->json(["data" => "Le client n'a pas été reçu."], 400);
        }

        $client = $this->clientRepository->find($commande->getClient()->getId());
        if (!$client instanceof Client) {
            return $this->json(["data" => "Le client n'existe pas."], 404);
        }

        $commande->setClient($client);

        $commande = $this->em->merge($commande);
        $this->em->flush();

        return $this->json($commande, 200);
    }

    /**
     * Ajoute une commande.
     *
     * @Route("/", methods={"POST"})
     */
    public function addOne(Request $request)
    {
        $content = $request->getContent();

        $commande = $this->serializerService->deserialize($content, Commande::class);

        if (!$commande->getClient() instanceof Client) {
            return $this->json(["data" => "Le client n'existe pas."], 400);
        }

        $client = $this->clientRepository->find($commande->getClient()->getId());
        if (!$client instanceof Client) {
            return $this->json(["data" => "Le client n'existe pas."], 400);
        }

        $commande->setClient($client);

        $this->em->persist($commande);
        $this->em->flush();

        return $this->json($commande, 200);
    }

    /**
     * Supprime une commande.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, methods={"DELETE"})
     */
    public function deleteOne($id)
    {
        $commande = $this->commandeRepository->find($id);
        if (!$commande instanceof Commande) {
            return $this->json([], 404);
        }

        $this->em->remove($commande);
        $this->em->flush();

        return $this->json(["data" => "ok"]);
    }

    /**
     * Enregistre un produit pour une commande
     *
     * @Route("/{idCommande}/produit", requirements={"idCommande" = "\d+"}, methods={"POST"})
     */
    public function ajouterProduit(Request $request, $idCommande)
    {
        $content = $request->getContent();

        // on vérifie que la commande existe
        $commande = $this->commandeRepository->find($idCommande);

        if (!$commande instanceof Commande) {
            return $this->json(["data" => "La commande n'existe pas."], 404);
        }

        $produit = $this->serializerService->deserialize($content, Produit::class);

        $produit->setCommande($commande);

        $this->em->persist($produit);

        $this->em->flush();

        return $this->json($produit);
    }

    /**
     * Enregistre plusieurs produits pour une commande.
     *
     * @Route("/{idCommande}/produits", requirements={"idCommande" = "\d+"}, methods={"POST"})
     */
    public function ajouterProduits(Request $request, $idCommande)
    {
        $content = $request->getContent();

        // on vérifie que la commande existe
        $commande = $this->commandeRepository->find($idCommande);

        if (!$commande instanceof Commande) {
            return $this->json(["data" => "La commande n'existe pas."], 404);
        }

        $produits = json_decode($content);

        // pour chaque produits reçu, on les ajoutes
        foreach ($produits as $oProduit) {
            $produit = $this->serializerService->deserialize(json_encode($oProduit), Produit::class);
            $produit->setCommande($commande);
            $this->em->persist($produit);
        }
        // on enregistre
        $this->em->flush();

        return $this->json($produit);
    }

    /**
     * Modifie un produit.
     *
     * @Route("/produit/{idProduit}", requirements={"idProduit" = "\d+"}, methods={"PUT"})
     */
    public function modifierProduit(Request $request, $idProduit)
    {
        $produit = $this->produitRepository->find($idProduit);
        if (!$produit instanceof Produit) {
            return $this->json(["data" => "Le produit n'existe pas."], 404);
        }

        $commande = $produit->getCommande();

        $produit = $this->serializerService->deserialize($request->getContent(), Produit::class);
        $produit->setId($idProduit);
        $produit->setCommande($commande);

        $produit = $this->em->merge($produit);

        $this->em->flush();

        return $this->json($produit);
    }

    /**
     * Supprime un produit.
     *
     * @Route("/produit/{idProduit}", requirements={"idProduit" = "\d+"}, methods={"DELETE"})
     */
    public function supprimerProduit($idProduit)
    {
        $produit = $this->produitRepository->find($idProduit);
        if (!$produit instanceof Produit) {
            return $this->json(["data" => "Le produit n'existe pas."], 404);
        }

        $this->em->remove($produit);
        $this->em->flush();

        return $this->json(["data" => "ok"]);
    }

    /**
     * Supprime tous les produits d'une commande.
     *
     * @Route("/{idCommande}/produit", requirements={"idCommande" = "\d+"}, methods={"DELETE"})
     */
    public function supprimerTousProduit($idCommande)
    {
        // on vérifie que la commande existe
        $commande = $this->commandeRepository->find($idCommande);

        if (!$commande instanceof Commande) {
            return $this->json(["data" => "La commande n'existe pas."], 404);
        }

        foreach ($commande->getProduits() as $produit) {
            $this->em->remove($produit);
        }
        $this->em->flush();

        return $this->json(["data" => "ok"]);
    }

    /**
     * Retourne les commandes de la date reçu.
     *
     * @Route("/telecharger/{date}", methods={"GET"})
     */
    public function telecharger(DateTime $date)
    {
        $id = strtoupper(base_convert($date->format("Ymd"), 10, 36));
        $html = $this->renderView('App:commande-pdf.twig', [
            "commandes" => $this->commandeRepository->findCommandesDuJour($date),
            "date" => $date,
            "id" => $id,
        ]);

        return $this->pdfService->getPdfResponse($html, "facture-{$date->format('Y-m-d')}.pdf", [
            "header-right" => "#{$id} - {$date->format('d/m/Y')} - [page]/[toPage]"
        ]);
    }
}
